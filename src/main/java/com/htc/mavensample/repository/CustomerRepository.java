package com.htc.mavensample.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.htc.mavensample.model.Customer;



@Repository
public interface CustomerRepository extends CrudRepository<Customer, String>
{
	
}
