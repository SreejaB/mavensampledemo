package com.htc.mavensample.repository;

import org.springframework.data.repository.CrudRepository;

import com.htc.mavensample.model.Policy;


public interface PolicyRepository extends CrudRepository<Policy, Long>
{

}
