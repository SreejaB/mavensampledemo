package com.htc.mavensample.service;


import java.util.List;
import java.util.Set;

import com.htc.mavensample.model.Customer;
import com.htc.mavensample.model.Policy;
import com.htc.mavensample.to.CustomerTO;
import com.htc.mavensample.to.PolicyTO;


public interface InsuranceService 
{
	public boolean addCustomer(CustomerTO customer);
	public boolean addCustomerWithPolicy(CustomerTO customerTO, PolicyTO policyTO);
	public boolean takeNewPolicy(CustomerTO customerTO,PolicyTO policyTO);
	public Set<Policy> getInsurancePolicies(String customerCode);
	public PolicyTO getInsurancePolicy(long policyno);
	public Customer getCustomerDetails(long policyno);

}
