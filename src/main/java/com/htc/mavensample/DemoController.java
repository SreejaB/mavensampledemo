package com.htc.mavensample;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DemoController 
{
	@RequestMapping(value = "hello")
	public String hello(){
        return "Spring boot Example";
    }

}
